const addTeacherBtns = document.querySelectorAll('#add-teacher-btn');
const teacherCards = document.querySelectorAll('.teacher-promo');
const closePopupBtns = document.querySelectorAll('.close');
const addTeacherPopup = document.querySelector('.add-teacher-popup');
const teacherInfoPopup = document.querySelector('.teacher-info-popup');

teacherCards.forEach((card) => {
    card.addEventListener('click', () => {
        teacherInfoPopup.classList.add('visible');
        document.body.style.overflow = 'hidden';
    });
});

addTeacherBtns.forEach((btn) => {
    btn.addEventListener('click', () => {
        addTeacherPopup.classList.add('visible');
        document.body.style.overflow = 'hidden';
    });
});

const closePopup = () => {
    addTeacherPopup.classList.remove('visible');
    teacherInfoPopup.classList.remove('visible');
};

closePopupBtns.forEach((btn) => {
    btn.addEventListener('click', () => {
        closePopup();
        document.body.style.overflow = '';
    });
});

addTeacherPopup.addEventListener('click', (e) => {
    if (e.target.classList.contains('add-teacher-popup')) {
        closePopup();
        document.body.style.overflow = '';
    }
});

teacherInfoPopup.addEventListener('click', (e) => {
    if (e.target.classList.contains('teacher-info-popup')) {
        closePopup();
        document.body.style.overflow = '';
    }
});

// LAB #3
const courses = ['Mathematics', 'Physics', 'English', 'Computer Science', 'Dancing', 'Chess',
    'Biology', 'Chemistry', 'Law', 'Art', 'Medicine', 'Statistics'];

// Task 1
function formatObjects(arrOfObjects, additionalObjects) {
    const formattedObjects = [];
    arrOfObjects.forEach((obj) => {
        const newObj = {};
        newObj.gender = obj.gender;
        newObj.title = obj.name.title;
        newObj.fullName = `${obj.name.first} ${obj.name.last}`;
        newObj.city = obj.location.city;
        newObj.state = obj.location.state;
        newObj.country = obj.location.country;
        newObj.postcode = obj.location.postcode;
        newObj.coordinates = {
            latitude: obj.location.coordinates.latitude,
            longitude: obj.location.coordinates.longitude,
        };
        newObj.timezone = {
            offset: obj.location.timezone.offset,
            description: obj.location.timezone.description,
        };
        newObj.email = obj.email;
        newObj.bDate = obj.dob.date;
        newObj.age = obj.dob.age;
        newObj.phone = obj.phone;
        newObj.pictureLarge = obj.picture.large;
        newObj.pictureThumbnail = obj.picture.thumbnail;
        newObj.id = '';
        newObj.favorite = null;
        newObj.course = courses[Math.floor(Math.random() * courses.length)];
        newObj.bgColor = '';
        newObj.note = 'Note';

        const randomAdditionalObject = additionalObjects[Math.floor(Math.random() * additionalObjects.length)];
        for (const key in randomAdditionalObject) {
            if (!newObj.hasOwnProperty(key)) {
                newObj[key] = randomAdditionalObject[key];
            }
        }

        formattedObjects.push(newObj);
    });
    return formattedObjects;
}

// Task 2
function isValidObject(object) {
    if ((typeof object.fullName !== 'string' || object.fullName[0] !== object.fullName[0].toUpperCase()) ||
        (typeof object.gender !== 'string' || object.gender[0] !== object.gender[0].toUpperCase()) ||
        (typeof object.note !== 'string' || object.note[0] !== object.note[0].toUpperCase()) ||
        (typeof object.state !== 'string' || object.state[0] !== object.state[0].toUpperCase()) ||
        (typeof object.city !== 'string' || object.city[0] !== object.city[0].toUpperCase()) ||
        (typeof object.country !== 'string' || object.country[0] !== object.country[0].toUpperCase())) {
            return false;
    }
    if (typeof object.age !== 'number') {
        return false;
    }
    if (!/^\+?[\d\(\)-]+$/.test(object.phone)) {
        return false;
    }
    if (!object.email.includes('@')) {
        return false;
    }
    return true;
}

// Task 3
function filterUsers(arrOfUsers, country, age, gender, favorite) {
    const filteredUsers = [];
    for (const user of arrOfUsers) {
        if (user.country === country && user.age === age &&
            user.gender === gender && user.favorite === favorite) {
                filteredUsers.push(user);
        }
    }
    return filteredUsers;
}

// Task 4
// sorted params are strings, ascending is boolean
function sortObjects(arrOfObjects, fullName, age, bDate, country, ascending) {
    const sortedParameter = fullName || age || bDate || country;
    if (ascending) {
        arrOfObjects.sort((a, b) => a[sortedParameter] > b[sortedParameter] ? 1 : -1);
    } else {
        arrOfObjects.sort((a, b) => a[sortedParameter] > b[sortedParameter] ? 1 : -1).reverse();
    }
}

// Task 5
function findObjects(arrOfObjects, searchParameter) {
    const foundedObjects = [];
    for (const obj of arrOfObjects) {
        for (const key in obj) {
            if (String(key) !== 'fullName' && String(key) !== 'note' && String(key) !== 'age') {
                continue;
            }
            if (obj[key] === searchParameter) {
                foundedObjects.push(obj);
            }
        }
    }
    return foundedObjects;
}

// Task 6
function getPersentOfFoundedObjects(allObjects, foundedObjects) {
    return Math.round((foundedObjects.length * 100) / allObjects.length);
}

// Display results
// insert here randomUserMock from 'FE4U-Lab3-mock.js'
const randomUserMock = [];

// insert here additionalUsers from 'FE4U-Lab3-mock.js'
const additionalUsers = [];

const formatted = formatObjects(randomUserMock, additionalUsers);
for (let i = 0; i < 5; i++) {
    console.log(`|---Object #${i + 1}---|`);
    console.log(formatted[i]);
}

// Validation
console.log('Validation');
console.log(isValidObject(formatted[4]));
formatted[4].fullName = 'mike';
console.log(isValidObject(formatted[4]));

const filteredUsers = filterUsers(formatted, 'Germany', 65, 'male', null);
console.log('Filtered users:');
console.log(filteredUsers);

// Sort by name ascending
console.log('\nSorted by name!');
sortObjects(formatted, 'fullName', '', '', '', true);
for (let i = 0; i < 5; i++) {
    console.log(formatted[i].fullName);
}

// Find users with age 47
const foundedObjcts = findObjects(formatted, 47);
console.log('\nFounded users:');
console.log(foundedObjcts);

console.log(`\n${getPersentOfFoundedObjects(formatted, foundedObjcts)}%`);
